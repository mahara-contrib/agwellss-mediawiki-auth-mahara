<?php
/**
 *Mediawiki Authentication Plugin for Mahara
 *Copyright (C) 2004 Brion Vibber <brion@pobox.com>
 *Copyright (C) 2010, 2011 Catalyst IT (http://www.catalyst.net.nz)
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; if not, write to the Free Software
 *Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
require_once('includes/AuthPlugin.php');

$wgExtensionCredits['other'][] = array(
    'name' => 'Mahara Authentication Plugin',
    'author' => 'Brett Wilkins',
    'url' => 'http://gitorious.org/mahara-contrib/mediawiki-auth-mahara',
    'description' => 'Authenticates against users in the Mahara database'
    );

class MaharaAuthPlugin extends AuthPlugin {
	/**
	 * Check whether there exists a user account with the given name.
	 * The name will be normalized to MediaWiki's requirements, so
     * you might need to munge it (for instance, for lowercase initial
	 * letters).
	 *
	 * @param $username String: username.
	 * @return bool
	 */

	var $dbname;
	var $host;
	var $prefix;
	var $dbtype;
	var $dbuser;
	var $dbpass;

    public function __construct($dbname, $host='localhost',$dbtype='', $user='', $password='', $prefix='') {
        $this->dbname = $dbname;
        $this->host = $host;
        $this->prefix = $prefix;
	    $this->dbtype = $dbtype;
        $this->dbuser = $user;
        $this->dbpass = $password;
    }

	public function userExists( $username ) {
        $username = strtolower($username);
        $db = $this->getDatabase();
        $sql = "SELECT username FROM ".$this->prefix."usr where LOWER(username) = '".$username."'";
        $res = $db->query($sql);
        $val = $db->fetchObject($res);
        $db->close();
        if (!empty($val)) {
            return true;
        }
		return false;
	}

	/**
	 * Check if a username+password pair is a valid login.
	 * The name will be normalized to MediaWiki's requirements, so
	 * you might need to munge it (for instance, for lowercase initial
	 * letters).
	 *
	 * @param $username String: username.
	 * @param $password String: user password.
	 * @return bool
	 */
	public function authenticate( $username, $password ) {
	$username = strtolower($username);
        $db = $this->getDatabase();
	    $sql = "SELECT username, password, salt FROM ".$this->prefix."usr where LOWER(username) = '".$username."' and deleted != 1";
        $res = $db->query($sql);
        $val = $db->fetchObject($res);
        $db->close();
        if (!empty($val)) {
            $passcheck = sha1($val->salt . $password);
            if ($passcheck == $val->password) {
                return true;
            }
        }
		return false;
	}

	/**
	 * Modify options in the login template.
	 *
	 * @param $template UserLoginTemplate object.
	 */
	public function modifyUITemplate( &$template ) {
		# Override this!
		$template->set( 'usedomain', false );
	}

	/**
	 * Set the domain this plugin is supposed to use when authenticating.
	 *
	 * @param $domain String: authentication domain.
	 */
	public function setDomain( $domain ) {
		$this->domain = $domain;
	}

	/**
	 * Check to see if the specific domain is a valid domain.
	 *
	 * @param $domain String: authentication domain.
	 * @return bool
	 */
	public function validDomain( $domain ) {
		# Override this!
		return true;
	}

	/**
	 * When a user logs in, optionally fill in preferences and such.
	 * For instance, you might pull the email address or real name from the
	 * external user database.
	 *
	 * The User object is passed by reference so it can be modified; don't
	 * forget the & on your function declaration.
	 *
	 * @param User $user
	 */
	public function updateUser( &$user ) {
        
        $db = $this->getDatabase();
	    $sql = "SELECT * FROM ".$this->prefix."usr where LOWER(username) = LOWER('".$user->mName."')";
        $res = $db->query($sql);
        $val = $db->fetchObject($res);
        $db->close();
		$user->setOption('nickname',$val->username);
		$user->setEmail($val->email);
		$user->setRealName($val->firstname.' '.$val->lastname);
        if ($val->admin == 1) {
            $user->addGroup('sysop');
        } else if (in_array('sysop',$user->getGroups())) {
            $user->removeGroup('sysop');
        }
		return true;
	}


	/**
	 * Return true if the wiki should create a new local account automatically
	 * when asked to login a user who doesn't exist locally but does in the
	 * external auth database.
	 *
	 * If you don't automatically create accounts, you must still create
	 * accounts in some way. It's not possible to authenticate without
	 * a local account.
	 *
	 * This is just a question, and shouldn't perform any actions.
	 *
	 * @return bool
	 */
	public function autoCreate() {
		return true;
	}

	/**
	 * Can users change their passwords?
	 *
	 * @return bool
	 */
	public function allowPasswordChange() {
		return false;
	}

	/**
	 * Set the given password in the authentication database.
	 * As a special case, the password may be set to null to request
	 * locking the password to an unusable value, with the expectation
	 * that it will be set later through a mail reset or other method.
	 *
	 * Return true if successful.
	 *
	 * @param $user User object.
	 * @param $password String: password.
	 * @return bool
	 */
	public function setPassword( $user, $password ) {
		return false;
	}

	/**
	 * Update user information in the external authentication database.
	 * Return true if successful.
	 *
	 * @param $user User object.
	 * @return bool
	 */
	public function updateExternalDB( $user ) {
		return false;
	}

	/**
	 * Check to see if external accounts can be created.
	 * Return true if external accounts can be created.
	 * @return bool
	 */
	public function canCreateAccounts() {
		return false;
	}

	/**
	 * Add a user to the external authentication database.
	 * Return true if successful.
	 *
	 * @param User $user - only the name should be assumed valid at this point
	 * @param string $password
	 * @param string $email
	 * @param string $realname
	 * @return bool
	 */
	public function addUser( $user, $password, $email='', $realname='' ) {
		return false;
	}


	/**
	 * Return true to prevent logins that don't authenticate here from being
	 * checked against the local database's password fields.
	 *
	 * This is just a question, and shouldn't perform any actions.
	 *
	 * @return bool
	 */
	public function strict() {
		return true;
	}

	/**
	 * Check if a user should authenticate locally if the global authentication fails.
	 * If either this or strict() returns true, local authentication is not used.
	 *
	 * @param $username String: username.
	 * @return bool
	 */
	public function strictUserAuth( $username ) {
		return true;
	}

	/**
	 * When creating a user account, optionally fill in preferences and such.
	 * For instance, you might pull the email address or real name from the
	 * external user database.
	 *
	 * The User object is passed by reference so it can be modified; don't
	 * forget the & on your function declaration.
	 *
	 * @param $user User object.
	 * @param $autocreate bool True if user is being autocreated on login
	 */
	public function initUser( &$user, $autocreate=false ) {
        return $this->updateUser($user);
	}

	/**
	 * If you want to munge the case of an account name before the final
	 * check, now is your chance.
	 */
	public function getCanonicalName( $username ) {
		return $username;
	}
	
	/**
	 * Get an instance of a User object
	 *
	 * @param $user User
	 * @public
	 */
	public function getUserInstance( User &$user ) {
		return new AuthPluginUser( $user );
	}

	private function getDatabase() {
		if (empty($this->dbtype)) {
			return false;
		}
		switch ($this->dbtype) {
			case 'mysql':
				return new DatabaseMysql($this->host,$this->dbuser,$this->dbpass,$this->dbname);
			case 'postgres':
			default:
				return new DatabasePostgres($this->host,$this->dbuser,$this->dbpass,$this->dbname);
		}
		return false;
	}
}
